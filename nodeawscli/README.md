Edit the `Dockerfile` and update the version of node

Run these command but use the version of node used in the Dockerfile

```
docker build -t poolplayers/awscli_node:18.17.0 -t poolplayers/awscli_node:latest .
docker push poolplayers/awscli_node --all-tags
```
