#!/usr/bin/env bash

docker build -t poolplayers/al2-x64-node:16 -t poolplayers/al2-x64-node:latest --target al2-x64-node .
docker build -t poolplayers/al2023-x64-node:18 -t poolplayers/al2023-x64-node:latest --target al2023-x64-node .
docker build -t poolplayers/al2023-arm64v8-node:18 -t poolplayers/al2023-arm64v8-node:latest --target al2023-arm64v8-node .
docker build -t poolplayers/deploy -t poolplayers/deploy:latest --target deploy .
