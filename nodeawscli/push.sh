#!/usr/bin/env bash

docker image push poolplayers/al2-x64-node --all-tags
docker image push poolplayers/al2023-x64-node --all-tags
docker image push poolplayers/al2023-arm64v8-node --all-tags
docker image push poolplayers/deploy --all-tags
