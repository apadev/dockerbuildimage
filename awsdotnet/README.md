Edit the `Dockerfile` and update the .Net version

This Docker image was built using [these instructions](https://aws.amazon.com/blogs/devops/extending-aws-codebuild-with-custom-build-environments-for-the-net-framework/) as a baseline

Run these command but use the version of .Net used in the Dockerfile
```
docker build -t poolplayers/awscli_node:10.15.3 -t poolplayers/awscli_node:latest .
docker push poolplayers/awscli_node:10.15.3
```
